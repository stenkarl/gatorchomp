this = {}

local function createImage(name, x, y, group, listener)
	local img = display.newImageRect("images/"..name..".png", 57, 52)
	img.x, img.y = x, y
	--img.xScale = 2
	--img.yScale = 2
	img:addEventListener("tap", listener)
	
	group:insert(img)
	
	return img
end

this.create = function( off, on, x, y, group)
	local offImage
	local onImage
	local function switch( event )
		onImage.isVisible = not onImage.isVisible
		offImage.isVisible = not offImage.isVisible
	end
	
	offImage = createImage(off, x, y, group, switch)
	onImage = createImage(on, x, y, group, switch)

	onImage.isVisible = false
	
	return onImage
end

return this