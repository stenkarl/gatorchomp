local this = {}

local physics = require "physics"

local bump = {media.newEventSound( "audio/bounce1.mp3" ),
				--media.newEventSound( "audio/bounce4.mp3" ),
				--media.newEventSound( "audio/bounce2.mp3" ),
				--media.newEventSound( "audio/bounce3.mp3" ) 
				}

local leftLeft
local leftOp
local leftRight
local leftVal

local rightLeft
local rightOp
local rightRight
local rightVal

local group = display.newGroup()

local ops = {}
local validOps = {}
local firstHit = false
local onFirstHit

local function createOp(name, eval)
	local op = {}
	op.name = name
	op.eval = eval
	
	return op
end

local function createOps()
	ops.plus = createOp( "Add", function(x, y) return x + y end)
	ops.minus = createOp( "Minus", function(x, y) return x - y end)
	ops.times = createOp( "Times", function(x, y) return x * y end)
	ops.divide = createOp( "Divide", function(x, y) return x / y end)
end

this.create = function(firstHitCallBack)
	onFirstHit = firstHitCallBack
	createOps()
	group = display.newGroup()

	return group
end

this.setValidOps = function(plus, minus, times, divide)
	validOps = nil
	validOps = {}
	if plus then table.insert(validOps, ops.plus) end
	if minus then table.insert(validOps, ops.minus) end
	if times then table.insert(validOps, ops.times) end
	if divide then table.insert(validOps, ops.divide) end
end

local function numValidOps()
    local n = 0
    for k, v in pairs(validOps) do
        n = n + 1
    end
    return n
end

local function playRandomBump()
	--local index = math.random(3)
	--media.playEventSound( bump[index] )
	media.playEventSound( bump[1] )

end

local function createImage ( x, scale, rotation, bounce, file )
	local image = display.newImageRect( "images/" .. file .. ".png", 100, 75)
	local halfH = image.height/2 * 0.8
	local halfW = (image.width / 2) * scale
	image.x, image.y = x, -100
	image.rotation = rotation
		
	local numShape = { -halfW,-halfH, halfW, -halfH, halfW, halfH, -halfW, halfH }
	physics.addBody( image, { density=1.0, friction=0.5, bounce=bounce, shape=numShape } )
	
	local function onCollide( event )
		if event.phase == "began" then
			if not firstHit then
				firstHit = true
				onFirstHit()
			end
			playRandomBump()
		end
	end
	
	image:addEventListener("collision", onCollide)
	
	return image
end

local function createNumber( which, x, randomRot )
	local scaleW = (which < 10 and 0.4 or 0.75)
	local rotation = 0
	if randomRot and which >= 10 then
		rotation = math.random(6) - 3
	end
	local bounce = (1 - which / 25) * 0.5
	--print ("which "..which..", rot "..rotation..", bounce "..bounce)
	local image = createImage(x, scaleW, rotation, bounce, which)
	image.value = which
		
	return image
end

local function createRandomNumber( x, randomRot)
	return createNumber(math.random(25), x, randomRot)
end

local function checkEqualValues(val1, val2, x)
	if val1 == val2 then
		local num = rightLeft.value + 1
		if num > 25 then num = 1 end
		rightLeft:removeSelf()
		rightLeft = nil
		rightLeft = createNumber (num, x, scaledW, true)
	end
	
end

local function createRandomOp()
	local which = math.random(numValidOps())
    
    return validOps[which]
end

local function eval(left, op, right)
	local val = op.eval(left.value, right.value)
	--print ("val " .. val)
	
	return val
end

local function createExpression( x, w )
	local op = createRandomOp()
	left = createRandomNumber(x, false)
	local opBounce = math.random(4) * 0.1
	opImage = createImage(x + w/2, 0.3, 0, opBounce, op.name) 
	opImage.op = op
	right = createRandomNumber(x + w, false)
	local val = eval(left, op, right)
	
	return left, opImage, right, val, op
end

local function nextExpression(x1, x2, w)
	leftLeft, leftOp, leftRight, leftVal = createExpression(x1 - w * 0.5, w)
	rightLeft, rightOp, rightRight, rightVal = createExpression(x2 - w * 0.5, w)
	
	checkEqualValues(leftVal, rightVal, x2 - w * 0.5)
	rightVal = eval(rightLeft, rightOp.op, rightRight)
	
	group:insert(leftLeft)
	group:insert(leftOp)
	group:insert(leftRight)
	group:insert(rightLeft)
	group:insert(rightOp)
	group:insert(rightRight)
end

local function nextNumber(x1, x2, w)
	local scaledW = w * 2
	leftLeft = createRandomNumber ( x1, scaledW, true)
	rightLeft = createRandomNumber ( x2, scaledW, true)
	
	checkEqualValues(leftLeft.value, rightLeft.value, x2)
	
	group:insert(leftLeft)
	group:insert(rightLeft)
	
	leftVal = leftLeft.value
	rightVal = rightLeft.value
end

this.next = function(x1, x2, w)
	if numValidOps() > 0 then
		nextExpression(x1, x2, w)
	else
		nextNumber(x1, x2, w)
	end
end 

this.leftGreater = function()
	return leftVal > rightVal
end

this.remove = function()
	firstHit = false
	if leftLeft then
		leftLeft:removeSelf()
	end
	if leftRight then
		leftRight:removeSelf()
	end
	if leftOp then
		leftOp:removeSelf()
	end
	leftLeft = Nil
	leftRight = Nil
	leftOp = Nil
	
	if rightLeft then
		rightLeft:removeSelf()
	end
	if rightRight then
		rightRight:removeSelf()
	end
	if rightOp then
		rightOp:removeSelf()
	end
	rightLeft = Nil
	rightRight = Nil
	rightOp = Nil	
end

this.destroy = function()
	group:removeSelf()
	group = nil
end

return this