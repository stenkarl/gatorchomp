local this = {}
local gator
local jaw
local halfScreenW = display.contentWidth / 2
local group
local initialY

this.create = function()
	gator = display.newImageRect( "images/GatorTop.png", 375, 425)
	jaw = display.newImageRect( "images/GatorJaw.png", 375, 125)
	
--	gator = display.newImage( "images/GatorTop.png")-
--	jaw = display.newImage( "images/GatorJaw.png")
	
	
	--print ("display "..display.contentWidth..","..display.contentHeight)
	--print ("gator "..gator.width..","..gator.height)
	--print ("jaw "..jaw.width..","..jaw.height)
	
	--jaw.y = gator.height - (jaw.height * 1.5)
	jaw.y = 30

	group = display.newGroup()
	group:insert(jaw)
	group:insert(gator)
	group.x = group.width / 2
	initialY = display.contentHeight
	group.y = initialY
	group.isVisible = false
	
	return group
end

this.reset = function()
	jaw.rotation = 90
	group.parent:insert(group)
	group.x, group.y = group.width * 2, initialY
end

this.moveDown = function( left )
	local endX = left and -100 or (display.contentWidth + 100)
   	this.onChomp()
	transition.to(group, {time=200, x=endX, y=display.contentHeight, 
						onComplete=this.onComplete})
end

this.move = function( left )
	group.isVisible = true	
	--left = true
	local toX
	local toY = display.contentHeight * 0.7

	if left then
		group.xScale = -1
		group.x = group.width * 2	
		toX = group.width / 2
	else
		group.xScale = 1
		group.x = -group.width / 2	
		toX = group.width * 0.9
	end
	
	transition.to(jaw, {time=800, rotation=-40}) --, onComplete=function () this.moveDown(left) end})
--	transition.to(jaw, {time=800, rotation=-25}) --, onComplete=function () this.moveDown(left) end})
--	transition.to(group, {time=900, x=toX, y=toY, transition=easing.inOutExpo})
	transition.to(group, {time=900, x=toX, y=toY, transition=easing.inOutExpo, onComplete = function () this.moveDown(left) end})

end

this.destroy = function()
	group:removeSelf()
	group = nil
	
	jaw = nil
	gator = nil
end

--this.move = function( left )
	--gator.xScale = left and -scale or scale
--	group.xScale = left and -scale or scale
--	local offset = 200
--	local toX = left and -offset or display.contentWidth + offset
--	transition.to(group, {time=1000, x=300, y=display.contentHeight * 0.7, 
--						transition=easing.inOutExpo, onComplete = function () this.moveDown(left) end})
--end

return this