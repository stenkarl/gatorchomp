local gameNetwork = require "gameNetwork"
local json = require "json"

local this = {}
local loggedIntoGC = false

local function initCallback( event )
    if event.data then
        loggedIntoGC = true
        --native.showAlert( "Success!", "User has logged into Game Center", { "OK" } )
    else
        loggedIntoGC = false
        --native.showAlert( "Fail", "User is not logged into Game Center", { "OK" } )
    end
end

this.init = function()
	gameNetwork.init( "gamecenter", initCallback )
end

local function requestCallback( event )
    local data = json.encode( event.data )
        
    -- show encoded json string via native alert
   -- native.showAlert( "event.data", data, { "OK" } )
end

this.unlock = function( trophy, percent )
	local trophyName = "GCTrophy"..trophy.."01"

	if percent > 100 then
		percent = 100
	end
	
	--native.showAlert( "unlocking achievement", trophy, { "OK" } )
	print ( "unlocking achievement:"..trophyName..", percent "..percent)

	if not loggedIntoGC then return end
		
	gameNetwork.request( "unlockAchievement", {
        achievement = {
            identifier=trophyName,
            percentComplete=percent,
            showsCompletionBanner=true,
        }
        --listener=requestCallback
	})	
	
end

this.requestTrophies = function( callback )
	if not loggedIntoGC then return end
	
	local function onRequest( event )
		local data = json.encode( event.data )
        
	    --native.showAlert( "event.data", data, { "OK" } )
	    callback (event.data)
	end
	gameNetwork.request( "loadAchievements", { listener=onRequest } )
	
end

this.reset = function()
	native.showAlert( "resetting achievements", "Achievements Reset", { "OK" } )
	if loggedIntoGC then gameNetwork.request( "resetAchievements" ) end
end

return this