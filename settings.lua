local storyboard = require( "storyboard" )
local gameCenter = require( "gamecenter" )

local scene = storyboard.newScene()

local widget = require "widget"
local toggle = require "toggle"

local plusButton
local minusButton
local timesButton
local divButton
local trophyW = 94

local addTrophyOn
local addTrophyOff
local subTrophyOn
local subTrophyOff
local multiplyTrophyOn
local multiplyTrophyOff
local divideTrophyOn
local divideTrophyOff

local function gotoGame()
	local options = {
    	effect = "fade",
    	time = 800,
    	params = { plus=plusButton.isVisible, minus=minusButton.isVisible, 
    				times=timesButton.isVisible, divide=divButton.isVisible}
	}
	storyboard.gotoScene( "game", options)
	
	return true
end

local function newButton( file, w, h, onRelease )
	button = widget.newButton {
		default="images/"..file.."1.png",
		over="images/"..file.."2.png",
		width=w, height=h,
		onRelease = onRelease
	}
--	button:setReferencePoint( display.CenterReferencePoint )
	button.x = display.contentWidth * 0.5
	button.y = display.contentHeight * 0.9
	
	return button
end

local function createTrophyImage(x, y, num, group, visible)
	local img = display.newImageRect("images/Trophy"..num..".png", trophyW, 108)
	img.x, img.y = x, y
	img.isVisible = visible
	
	group:insert(img)
	
	return img
end

local function createTrophy( x, y, group )
	local off = createTrophyImage(x, y, 1, group, true)
	local on = createTrophyImage(x, y, 2, group, false)	
	
	return on, off
end

local function createTrophies(row, col1, col2, col3, col4, group)
	addTrophyOn, addTrophyOff = createTrophy(col1, row, group)
	subTrophyOn, subTrophyOff = createTrophy(col2, row, group)
	multiplyTrophyOn, multiplyTrophyOff = createTrophy(col3, row, group)
	divideTrophyOn, divideTrophyOff = createTrophy(col4, row, group)	
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	local background = display.newImageRect( "images/Settings.png", display.contentWidth, display.contentHeight )
--	background:setReferencePoint( display.TopLeftReferencePoint )
	background.anchorX = 0
	background.anchorY = 0
	--background.x, background.y = 0, 0
	
	playBtn = newButton("play", 89, 39, gotoGame)
		
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( playBtn )
	
	local margin = 5
	local gap = (display.contentWidth - (margin * 2)) / 4
	local initialX = margin + trophyW / 2
	local row = display.contentHeight * 0.55
	local col1 = initialX
	local col2 = initialX + gap
	local col3 = initialX + gap * 2
	local col4 = initialX + gap * 3
	
	createTrophies(row, col1, col2, col3, col4, group)

	plusButton = toggle.create( "Add1", "Add2", col1, row, group )
	minusButton = toggle.create( "Minus1", "Minus2", col2, row, group )
	timesButton = toggle.create( "Multiply1", "Multiply2", col3, row, group )
	divButton = toggle.create( "Divide1", "Divide2", col4, row, group )

end

local function updateTrophies( data )
	for i,t in ipairs(data) do
		if t.isCompleted and t.identifier == "GCTrophyAdd01" then
			print ("earned add trophy")
			addTrophyOn.isVisible = true
		end
		if t.isCompleted and t.identifier == "GCTrophySubtract01" then
			print ("earned sub trophy")
			subTrophyOn.isVisible = true
		end	
		if t.isCompleted and t.identifier == "GCTrophyMultiply01" then
			print ("earned multiply trophy")
			multiplyTrophyOn.isVisible = true			
		end	
		if t.isCompleted and t.identifier == "GCTrophyDivide01" then
			print ("earned divide trophy")
			divideTrophyOn.isVisible = true
			
		end		
	end
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	gameCenter.requestTrophies( updateTrophies )
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
		
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()
		playBtn = nil
	end
	
	group:removeSelf()
	group = nil
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene