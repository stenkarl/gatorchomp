local gameCenter = require "gamecenter"
 
-- function to listen for system events
local function onSystemEvent( event ) 
    if event.type == "applicationStart" then
        gameCenter.init()
        return true
    end
end
Runtime:addEventListener( "system", onSystemEvent )
display.setStatusBar( display.HiddenStatusBar )

local deviceWidth = ( display.contentWidth - (display.screenOriginX * 2) ) / display.contentScaleX
local scaleFactor = math.floor( deviceWidth / display.contentWidth )
print( "scale:"..scaleFactor )

print ("w:"..display.contentWidth..", h:"..display.contentHeight)

function showFps()
        local prevTime = 0
        local curTime = 0
        local dt = 0       
        local fps = 50
        local mem = 0
              
        local underlay = display.newRoundedRect(0, 0, 300, 20, 12)   
        underlay.x = 240
        underlay.y = 11
        underlay:setFillColor(0, 0, 0, 128)             
        local displayInfo = display.newText("FPS: " .. fps .. " - Memory: ".. mem .. "mb", 120, 2, native.systemFontBold, 16)
        
        local function updateText()
                curTime = system.getTimer()
                dt = curTime - prevTime
                prevTime = curTime
                fps = math.floor(1000 / dt)
                mem = system.getInfo("textureMemoryUsed") / 1000000
                
                --Limit fps range to avoid the "fake" reports
                if fps > 60 then
                        fps = 60
                end
                
                displayInfo.text = "FPS: " .. fps .. " - Memory: ".. string.sub(mem, 1, string.len(mem) - 4) .. "mb"
                underlay:toFront()
                displayInfo:toFront()
        end
        
        Runtime:addEventListener("enterFrame", updateText)
end
--showFps()
-- include the Corona "storyboard" module
local storyboard = require "storyboard"

-- load menu screen
storyboard.gotoScene( "menu" )
--storyboard.gotoScene( "splashscreen" )