local storyboard = require( "storyboard" )
local widget = require( "widget" )
local statusBar = require( "statusBar" )
local gator = require( "gator" )
local expression = require( "expression" )
local pad = require( "pad" )
local score = require ( "score" )
local physics = require "physics"
local gameCenter = require( "gamecenter" )

local scene = storyboard.newScene()
local padLeft, padRight
local status
local totalChomps = 0
local numChomps = 0
local levelNum = 1
local menuButton

local screenW, screenH = display.contentWidth, display.contentHeight
local halfScreenW = screenW / 2
local quarterW = screenW / 4
local threeQuarterW = screenW * 3 / 4
local scoreY = display.contentHeight * 0.05

local chomp = audio.loadSound( "audio/Chomp2.mp3" )
local oops = audio.loadSound( "audio/Incorrect.mp3" )
local youDidIt = audio.loadSound( "audio/LevelComplete.mp3" )
local levelCompleteImg
local levelCompleteText
local curLevelText
local numChompsText
local curChompsText
local roundInProgress = false
local levelCompleteGroup
local operationType

local numIncorrect = 0
local numCorrect = 0
local perfectLevel = true

physics.start(); physics.pause()

--physics.setDrawMode("hybrid")

local function createBackground()
	local background = display.newImageRect( "images/Background.png", display.contentWidth, display.contentHeight )
	--background:setReferencePoint( display.TopLeftReferencePoint )
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = 0, 0
	
	return background
end

local function createBound( x, w )
	local b = display.newRect(x, 100, w, display.contentHeight * 0.5)
	b.isVisible = false
	physics.addBody( b, "static", { friction=0.3 } )
	
	return b
end

local function createBounds()
	local group = display.newGroup()
	group:insert(createBound ( display.contentWidth * 0.01, 2 ))
	group:insert(createBound ( halfScreenW - 15 / 2, 15 ))
	group:insert(createBound ( display.contentWidth * 0.99, 2 ))
	
	return group
end

local function onChomp()
	expression.remove()
end

local function nextLevel()
	levelNum = levelNum + 1
	print("levelNum "..levelNum..", numChomps "..numChomps)
	transition.to(levelCompleteGroup, {time = 200, alpha=0})
	score.reset()
	numChomps = 0
	perfectLevel = true

	nextRound()
end

local function startStatus()
	statusBar.resetStatus()
	roundInProgress = true
end

local function checkTrophy()	
	if operationType then
		gameCenter.unlock(operationType, levelNum * 10)
	end
	if perfectLevel then
		print ("perfect level")
		gameCenter.unlock("Perfect", 100)
	end
end

local function onLevelComplete()
    audio.play( youDidIt )
    curLevelText.text = levelNum
    curChompsText.text = numChomps
    transition.to(levelCompleteGroup, {time = 200, alpha=1})
    
    checkTrophy()

    timer.performWithDelay(2000, nextLevel)
end

local function gotoMenu()
	storyboard.gotoScene( "menu", "fade", 500 )
end

local function createLevelComplete( group )
	levelCompleteImg = display.newImageRect("images/LevelComplete.png", 354, 157)
	
	levelCompleteImg.x, levelCompleteImg.y = halfScreenW, display.contentHeight * 0.3
	
	local imgW, imgH = 325, 40
	local lvlY = display.contentHeight * 0.6
	levelCompleteText = display.newImageRect("images/GCLevel.png", imgW, imgH)
	levelCompleteText.x, levelCompleteText.y = halfScreenW, lvlY
	numChompsText = display.newImageRect("images/GCChomps.png", imgW, imgH)
	local chompsY = lvlY + imgH
	numChompsText.x, numChompsText.y = halfScreenW, chompsY
	
	local textX, textX2 = halfScreenW + 42, halfScreenW + 52
	local textY, textY2 = lvlY - 18, chompsY - 18
	local fontSize = 30
	--local gradient = graphics.newGradient({ 153, 202, 60}, {255, 242, 131}, { 140, 188, 63 }, "down" )
	curLevelText = display.newRetinaText(""..levelNum, textX, textY, native.systemFont, fontSize)
	curChompsText = display.newRetinaText(""..numChomps, textX2, textY2, native.systemFont, fontSize)
	
	--curLevelText:setTextColor(gradient)	
	--curChompsText:setTextColor(gradient)
	
	levelCompleteGroup = display.newGroup()				

	levelCompleteGroup:insert(levelCompleteImg)
	levelCompleteGroup:insert(levelCompleteText)
	levelCompleteGroup:insert(numChompsText)
	levelCompleteGroup:insert(curLevelText)
	levelCompleteGroup:insert(curChompsText)	
	
	levelCompleteGroup.alpha = 0
	
	group:insert(levelCompleteGroup)
end

local function createBackToMenu()	
	menuButton = widget.newButton{
		default="images/back1.png",
		over="images/back2.png",
		width=35, height=34,
		onRelease = gotoMenu
	}
	--menuButton:setReferencePoint( display.TopLeftReferencePoint )
	menuButton.anchorX = 0
	menuButton.anchorY = 0
	return menuButton
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	group:insert(createBackground())
	
	group:insert(createBounds())
	local padW = screenW * 0.45
	local padY = display.contentHeight * 0.7
	local padH = 38
	padLeft = pad.create( quarterW, padY, padW, padH )
	padRight = pad.create( threeQuarterW, padY, padW, padH )
	
	group:insert(gator.create())
	gator.onComplete = nextRound
	gator.onChomp = onChomp
	
	group:insert(statusBar.create(halfScreenW, halfScreenW, display.contentHeight * 0.15, nextRound))
	group:insert(score.create(scoreY))
	group:insert(createBackToMenu())	
			
	-- all display objects must be inserted into group
	group:insert( padLeft )
	group:insert( padRight)
	group:insert( expression.create(startStatus) )
	createLevelComplete(group)

end

local function getOperationType( params )
	if params.plus and not params.minus and not params.times and not params.divide then
		return "Add"
	elseif not params.plus and params.minus and not params.times and not params.divide then
		return "Subtract"
	elseif not params.plus and not params.minus and params.times and not params.divide then
		return "Multiply"
	elseif not params.plus and not params.minus and not params.times and params.divide then
		return "Divide"
	end
	return nil
end

function nextRound()
	if score.isLevelComplete() then
		onLevelComplete()
		return
	end
	numChomps = numChomps + 1
	totalChomps = totalChomps + 1
	expression.next(quarterW, threeQuarterW,quarterW)
	gator.reset()
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	levelNum = 1
	numChomps, numIncorrect, numCorrect = 0, 0, 0
	score.reset()

	expression.remove()
	local params = event.params
	operationType = getOperationType( event.params )
	expression.setValidOps(params.plus, params.minus, params.times, params.divide)
	physics.start()	

	Runtime:addEventListener("tap",onTap)
	
	nextRound()	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	--print ("exit scene: game")
	statusBar.cancel()
	Runtime:removeEventListener("tap",onTap)
	expression.remove()
	
	physics.pause()	
end

local function checkCorrectTrophies()
	print ("correct "..numCorrect..", incorrect "..numIncorrect)
	
	if numCorrect == 10 then
		gameCenter.unlock("Long", 100)
	elseif numIncorrect == 5 then
		gameCenter.unlock("Sad", 100)
	end
end

function onTap( event )
    --print ("tap x" .. event.x .. " y " .. event.y)

	if event.x < 35 and event.y < 35 then
		gotoMenu()
		return
	end
	
	if not roundInProgress then
		return
	end
	roundInProgress = false
	statusBar.cancel()

    local guessedLeft = event.x < halfScreenW
    local leftGreater = expression.leftGreater()

    gator.move(leftGreater)

    if (guessedLeft and leftGreater) or (not guessedLeft and not leftGreater) then
    	numIncorrect = 0
    	numCorrect = numCorrect + 1
    	audio.play( chomp )
    	score.add(statusBar.remainingStatus())
    	statusBar.rise(scoreY)
    else
    	perfectLevel = false
    	numCorrect = 0
    	numIncorrect = numIncorrect + 1
    	audio.play( oops )
    	statusBar.drop()
    end
    checkCorrectTrophies()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	if menuButton then
		menuButton:removeSelf()
		menuButton = nil
	end

	local group = self.view()
	gator.destroy()
	gator = nil
	statusBar.destroy()
	statusBar = nil
	physics.stop()
	physics = nil
	
	expression.destroy()
	expression = nil
	
	group:removeSelf()
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene