local this = {}

local physics = require "physics"

this.create = function( x, y, w, h )
	local padHeight = h
	local padWidth = w
	local padHalfWidth = padWidth / 2
	local pad = display.newImageRect( "images/Lilypad.png", padWidth, padHeight )
	--local pad = display.newImage( "images/Lilypad.png" )

	pad.x, pad.y = x, y
	
	local padShape = { -padHalfWidth,-5, padHalfWidth,-5, padHalfWidth, 5, -padHalfWidth, 5 }
	physics.addBody( pad, "static", { friction=0.3, shape=padShape } )
	
	return pad
end

return this