local this = {}
local width
local timerId
local s
local y
local onComplete
local timerStep = 3

this.remainingStatus = function()
	return s.width / width
end

this.resetStatus = function()
	s.y = y
	s.alpha = 1
	s.width = width
	
	timerId = timer.performWithDelay(50, this.updateStatus, width / timerStep)
end

this.updateStatus = function()
	s.width = s.width - timerStep
	if (s.width <= timerStep) then
		s.alpha = 0
	end
end

this.create = function ( x, w, h, callback )
	onComplete = callback
	width = w
	y = h
	s = display.newImageRect( "images/TimeBar.png", 250, 17)
	s.x, s.y = x, y
	
	return s
end

this.cancel = function() 
	if timerId then
   		timer.cancel(timerId)
   		timerId = nil
   	end
end

this.rise = function(y)
	transition.to(s, {time = 200, y = y, alpha = 0})
end

this.drop = function()
	transition.to(s, {time = 500, y = display.contentHeight * 1.1, alpha=0}) 
end

this.destroy = function()
	if timerId then
		timerId = nil
	end
end

return this