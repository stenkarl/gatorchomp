local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local widget = require "widget"
local gameCenter = require( "gamecenter" )

local playButton
local howButton
local background

local function gotoSettings()
	storyboard.gotoScene( "settings", "slideLeft", 500 )
	
	return true
end

local function gotoTutorial()
	--gameCenter.reset()
	storyboard.gotoScene( "help", "slideRight", 500 )
	
	return true
end

local function newButton( file, x, w, h, onRelease )
	button = widget.newButton {
		default="images/"..file.."1.png",
		over="images/"..file.."2.png",
		width=w, height=h,
		onRelease = onRelease
	}
--	button:setReferencePoint( display.CenterReferencePoint )
	button.x = x
	button.y = display.contentHeight * 0.5
	
	return button
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	background = display.newImageRect( "images/GatorChompTitle.png", display.contentWidth, display.contentHeight )
	--background:setReferencePoint( display.TopLeftReferencePoint )
	background.anchorX = 0
	background.anchorY = 0
	--background.x, background.y = 0, 0

	playButton = newButton("play", display.contentWidth * 0.25, 89, 39, gotoSettings)
	howButton = newButton("How", display.contentWidth * 0.75, 89, 32, gotoTutorial)
	
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( playButton )
	group:insert( howButton )
	--group.anchorChildren = true
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	--storyboard.purgeScene( "game" )

end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playButton then
		playButton:removeSelf()	-- widgets must be manually removed
		playButton = nil
	end
	if howButton then
		howButton:removeSelf()
		howButton = nil
	end
	background:removeSelf()
	background = nil
	group:removeSelf()
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene