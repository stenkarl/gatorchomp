local this = {}

local background
local score
local width
local x
local levelComplete = false

this.create = function(y)
	x = display.contentWidth * 0.25
	width = display.contentWidth / 2
	background = display.newImageRect( "images/ProgressBar.png", 250, 17)
	--background:setReferencePoint(display.TopLeftReferencePoint)
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = x, y
	
	local offsetX = 20
	score = display.newImageRect("images/ProgressBarGreen.png", 
									background.width - offsetX, background.height)
	--score:setReferencePoint(display.TopLeftReferencePoint)
	score.anchorX = 0
	score.anchorY = 0
	score.x, score.y = x + offsetX / 2, y
	score.xScale = 0.01
	score.isVisible = false

	--print ("x,width "..score.x..","..score.width)
	
	local group = display.newGroup()
	group:insert(background)
	group:insert(score)
	
	return group
end

this.reset = function()
	transition.to(score, {time = 400, xScale=0.01})
	levelComplete = false
	score.isVisible = false
end

this.isLevelComplete = function()
	return levelComplete
end

this.add = function( amount )
	score.isVisible = true
	local offset = 0.95 * amount
	local newScale = score.xScale + offset
	if newScale >= 1.0 then
		newScale = 1.0
		levelComplete = true	
	end	
	transition.to(score, {time = 200, xScale=newScale})	
	--print ("scale,offset "..score.xScale..","..offset)
end

return this